# epics-archiver
Small Docker container version for easy deployment of EPICS Archiver.

This is just a way to backup a working version/configuration of EPICS archiver appliance cloned from [pklaus Docker image of the EPICS Archiver Appliance](https://github.com/pklaus/docker-archiver-appliance)

## Instructions
Edit the docker compose file to reflect storage and hostname of the host machine.

To access:

 * Manager: http://hostname:17665/mgmt/ui/index.html
 * Viewer: http://hostname:17668/retrieval/ui/viewer/archViewer.html
